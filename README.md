# OnMyShelf import module for dvdfr.com

Module OnMyShelf pour import de Blu-Rays/DVDs depuis le site https://dvdfr.com

# Installation
Aller dans le dossier `modules/import` et cloner ce repository:
```bash
git clone https://gitlab.com/onmyshelf/modules/import/dvdfr
```

# Mise à jour
Aller dans le dossier `modules/import/dvdfr` et exécuter:
```bash
git pull
```

# Aide
Besoin d'aide sur ce module ? [Créez un ticket ici](https://gitlab.com/onmyshelf/modules/import/dvdfr/-/issues).

Plus d'informations sur OnMyShelf ici: https://onmyshelf.app
