<?php

require_once('inc/import/global/html.php');

class Import extends HtmlImport
{
    public function load()
    {
        $this->website = 'https://www.dvdfr.com';

        $this->properties = [
            'source',
            'title',
            'subtitle',
            'image',
            'summary',
            'time',
            'director',
            'actors',
        ];

        return true;
    }


    /**
     * Search item
     * @return array Array of results
     */
    public function search($search)
    {
        $this->source = $this->website."/search/multisearch.php?multiname=".preg_replace('/\s/', '+', $search);
        $this->loadHtml();

        $results = [];
        foreach ($this->html->find('.singleResult') as $product) {
            // main entry
            $results[] = [
                'source' => $this->urlFullPath($this->getHtml('h2 a', $product, 'href')),
                'name' => $this->getText($this->getHtml('h2 a', $product)),
                'image' => $this->getHtml('.left .imghover img', $product, 'data-lazyload'),
                'description' => $this->getText($this->getHtml('.compact .left', $product)),
            ];

            // other editions
            foreach ($product->find('.autresEditions tr') as $edition) {
                $results[] = [
                    'source' => $this->urlFullPath($this->getHtml('td.title a', $edition, 'href')),
                    'name' => $this->getText($this->getHtml('td.title a', $edition)),
                    'description' => $this->getText($this->getHtml('td.releaseDate', $edition)),
                ];
            }
        }

        return $results;
    }


    /**
     * Get item data from source
     * @return array Array of item properties
     */
    public function getData()
    {
        $this->loadHtml();

        return [
            'source' => $this->source,
            'title' => $this->getText($this->getHtml('#titrefilm')),
            'subtitle' => $this->getText($this->getHtml('#soustitrefilm')),
            'image' => $this->download(preg_replace("/^.*zoomImage: '(.*)'/", '$1', $this->getHtml('#coverZoom', null, 'data-cloudzoom'))),
            'summary' => preg_replace('/^Synopsis : /', '', $this->getText($this->getHtml('#colonnecentre p'))),
            'time' => substr($this->getHtml('#editeur time', null, 'datetime'), 1, -1), // transform P120M -> 120
            'director' => $this->getList($this->getHtml('#cast1 ul')),
            'actors' => $this->getList($this->getHtml('#cast2 ul')),
        ];
    }
}
